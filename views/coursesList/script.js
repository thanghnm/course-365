$(document).ready(function () {
  "usetrict"
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gBASE_URL = "https://624abe0dfd7e30c51c110ac6.mockapi.io/api/v1/";
  const gPRODUCT_COLUMNS = ["id", "courseCode", "courseName", "price", "discountPrice", "duration", "level", "teacherName", "action"]
  const gCOLUMN_STT = 0
  const gCOLUMN_COURSE_CODE = 1
  const gCOLUMN_COURSE_NAME = 2
  const gCOLUMN_COURSE_PRICE = 3
  const gCOLUMN_DISCOUNT_PRICE = 4
  const gCOLUMN_COURSE_DURATION = 5
  const gCOLUMN_COURSE_LEVEL = 6
  const gCOLUMN_TEACHER_NAME = 7
  const gCOLUMN_ACTION = 8
  var gStt = 0
  var gTable = $("#table-product").DataTable({
    // Phân trang
    paging: true,
    // Tìm kiếm
    searching: false,
    // Sắp xếp
    ordering: false,
    // Số bản ghi phân trang
    lengthChange: false,
    columns: [
      { data: gPRODUCT_COLUMNS[gCOLUMN_STT] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COURSE_CODE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COURSE_NAME] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COURSE_PRICE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_DISCOUNT_PRICE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COURSE_DURATION] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COURSE_LEVEL] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_TEACHER_NAME] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_ACTION] }


    ],
    columnDefs: [
      {
        targets: gCOLUMN_STT,
        render: renderStt
      },
      {
        targets: gCOLUMN_ACTION,
        defaultContent: `<button class="btn btn-edit">
        <i class="fa-solid fa-pen-to-square"></i>
        </button>
        <button class="btn btn-delete">
        <i class="fa-solid fa-trash-can"></i>
        </button>`
      },
    ]
  })
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()
  // gán sự kiện cho nút Thêm khóa học
  $(document).on("click", "#btn-add-courses", function () {
    $("#add-modal").modal("show")
  })
  // Gán sự kiện cho nút xác nhận thêm khóa học
  $(document).on("click", "#btn-confirm-add-course", function () {
    onBtnAddCoursesClick()
  })
  // Gán sự kiện cho nút edit
  $(document).on("click", ".btn-edit", function () {
    onBtnEditClick(this)
  })
  // Gán sự kiện cho nút update product trong modal edit
  $(document).on("click", "#btn-confirm-edit-course", function () {
    onBtnUpdateCourseClick()
  })
  // Gán sự kiện cho nút Delete
  $(document).on("click", ".btn-delete", function () {
    onBtnDeleteClick(this)
  })
  // Gán sự kiện cho nút confirm delete
  $(document).on("click", "#btn-confirm-delete-course", function () {
    onBtnConfirmDeleteClick()
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Gọi Api
    processApiToLoadListCourses()
  }
  function onBtnAddCoursesClick() {
    var vCourse = {
      courseCode: "",
      courseName: "",
      level: "",
      duration: "",
      price: 0,
      discountPrice: 0,
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: false,
      isTrending: false,
    }
    // B1 Thu thập dữ liệu
    getDataToAddCourse(vCourse)
    // B2 Kiểm tra dữ liệu
    var vValidate = isCourseDataValidate(vCourse)
    if (vValidate == true) {
      //B3 Gọi Api để thêm vào danh sách
      processApiToAddCourse(vCourse)
    }

  }
  function onBtnUpdateCourseClick() {
    var vCourse = {
      courseCode: "",
      courseName: "",
      duration: "",
      price: 0,
      discountPrice: 0,
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: false,
      isTrending: false,
    }
    // B1 Thu thập dữ liệu
    getDataToUpdateCourse(vCourse)
    // B2 kiểm tra dữ liệu
    var vValidate = isCourseDataValidate(vCourse)
    if (vValidate == true) {
      // B3 Gọi Api và xử lý hiển thị
      processApiToUpdateCourse(vCourse)
    }

  }
  function onBtnConfirmDeleteClick(paramButton) {
    // B1 Lấy Id của course cần xóa
    var vCourseID = $("#btn-confirm-delete-course").attr("courseId")
    // B2 Kiểm tra dữ liệu[không có]
    // B3 Gọi Api xóa khóa học
    processApiToDeleteCourse(vCourseID)
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm render Cột số thứ tự
  function renderStt() {
    gStt++
    return gStt
  }
  // Hàm gọi Api để lấy dữ liệu từ server cho việc hiển thị ra site
  function processApiToLoadListCourses() {
    gStt = 0
    $.ajax({
      url: gBASE_URL + "/courses/",
      type: "GET",
      success: function (responseObject) {
        displayDataToTable(responseObject)
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm hiển thị table
  function displayDataToTable(paramObject) {
    gTable.clear();
    gTable.rows.add(paramObject);
    gTable.draw();
  }
  // Hàm thu thập dữ liệu để thêm mới khóa học
  function getDataToAddCourse(paramObject) {
    paramObject.courseCode = $.trim($("#input-add-course-code").val())
    paramObject.courseName = $.trim($("#input-add-course-name").val())
    paramObject.duration = $.trim($("#input-add-course-duration").val())
    paramObject.level = $("#select-add-level").val()
    paramObject.coverImage = $.trim($("#img-add-url").val())
    paramObject.price = $.trim($("#input-add-course-price").val())
    paramObject.discountPrice = $.trim($("#input-add-course-discount-price").val())
    paramObject.teacherName = $.trim($("#input-add-teacher-name").val())
    paramObject.teacherPhoto = $.trim($("#input-add-teacher-photo").val())
    // console.log(paramObject)
  }
  // Hàm kiểm tra dữ liệu
  function isCourseDataValidate(paramObject) {
    if (paramObject.courseCode == "") {
      alert("Hãy nhập mã khóa học")
      return false
    }
    if (paramObject.courseName == "") {
      alert("Hãy nhập tên khóa học")
      return false
    }
    if (paramObject.duration == "") {
      alert("Hãy nhập thời lượng khóa học")
      return false
    }
    if (paramObject.teacherName == "") {
      alert("Hãy nhập tên giáo viên")
      return false
    }
    if (paramObject.teacherPhoto == "") {
      alert("Hãy nhập tên giáo viên")
      return false
    }
    if (paramObject.coverImage == "") {
      alert("Hãy nhập đường dẫn hình ảnh")
      return false
    }
    if (paramObject.price == "") {
      alert("Hãy nhập giá")
      return false

    }
    if (paramObject.discountPrice == "") {
      alert("Hãy nhập giá khuyến mãi")
      return false

    }
    return true
  }
  // Hàm gọi Api để thêm khóa học
  function processApiToAddCourse(paramObject) {
    $.ajax({
      url: gBASE_URL + "/courses/",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (response) {
        console.log(response)
        // B4: xử lý front-end
        alert(`
            Thêm Khóa học thành công`)
        $("#add-modal").modal("hide")
        onPageLoading()

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
  // Hàm xử lý khi click nút edit
  function onBtnEditClick(paramBtn) {
    $("#edit-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    // console.log(vDataRow)
    $("#input-edit-course-code").val(vDataRow.courseCode)
    $("#input-edit-course-name").val(vDataRow.courseName)
    $("#input-edit-course-duration").val(vDataRow.duration)
    $("#input-edit-teacher-name").val(vDataRow.teacherName)
    $("#input-edit-teacher-photo").val(vDataRow.teacherPhoto)
    $("#select-edit-level").val(vDataRow.level)
    $("#img-edit-url").val(vDataRow.coverImage)
    $("#input-edit-course-price").val(vDataRow.price)
    $("#input-edit-course-discount-price").val(vDataRow.discountPrice)
    $("#btn-confirm-edit-course").attr("courseId", vDataRow.id)
  }
  // Hàm thu thập dữ liệu để thêm mới khóa học
  function getDataToUpdateCourse(paramObject) {
    paramObject.courseCode = $.trim($("#input-edit-course-code").val())
    paramObject.courseName = $.trim($("#input-edit-course-name").val())
    paramObject.duration = $.trim($("#input-edit-course-duration").val())
    paramObject.level = $("#select-edit-level").val()
    paramObject.coverImage = $.trim($("#img-edit-url").val())
    paramObject.price = $.trim($("#input-edit-course-price").val())
    paramObject.discountPrice = $.trim($("#input-edit-course-discount-price").val())
    paramObject.teacherName = $.trim($("#input-edit-teacher-name").val())
    paramObject.teacherPhoto = $.trim($("#input-edit-teacher-photo").val())
    // console.log(paramObject)
  }
  // Hàm gọi Api để update product
  function processApiToUpdateCourse(paramObject) {
    $.ajax({
      url: gBASE_URL + "/courses/" + $("#btn-confirm-edit-course").attr("courseId"),
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (responseObject) {
        alert("Update Khóa học thành công")
        // Ẩn modal edit
        $("#edit-modal").modal("hide")
        // Load lại bảng
        processApiToLoadListCourses()
        // B4 xử lý hiển thị
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm xử lý khi click nút delete
  function onBtnDeleteClick(paramBtn) {
    $("#delete-confirm-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    $("#btn-confirm-delete-course").attr("courseId", vDataRow.id)
  }
  // Hàm gọi Api xóa Sản phẩm
  function processApiToDeleteCourse(paramCourseId) {
    $.ajax({
      url: gBASE_URL + "/courses/" + paramCourseId,
      type: "DELETE",
      success: function (response) {
        // Xử lý hiển thị
        alert("xóa khóa học thành công");
        // Ẩn modal xóa
        $("#delete-confirm-modal").modal("hide")
        // Load lại bảng
        processApiToLoadListCourses()
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });

  }
})