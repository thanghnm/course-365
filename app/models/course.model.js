const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const courseSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },

    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        }
    ]
}, {
    timestamps: true
});

module.exports = mongoose.model("Course", courseSchema);