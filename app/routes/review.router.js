const express = require("express");

const router = express.Router();

const {
    getAllReviewMiddleware,
    createReviewMiddleware,
    getReviewByIDMiddleware,
    deleteReviewMiddleware,
    updateReviewMiddleware
} = require("../middlewares/review.middleware");
const { createReviewOfCourse } = require("../controllers/review.controller");


router.get("/", getAllReviewMiddleware, (req, res) => {
    res.json({
        message: "GET all reviews"
    })
});

router.post("/", createReviewMiddleware, createReviewOfCourse)

router.get("/:reviewid", getReviewByIDMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.json({
        message: "GET review id = " + reviewid
    })
})

router.put("/:reviewid", updateReviewMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.json({
        message: "PUT review id = " + reviewid
    })
})

router.delete("/:reviewid", deleteReviewMiddleware, (req, res) => {
    const reviewid = req.params.reviewid;

    res.json({
        message: "DELETE review id = " + reviewid
    })
})

module.exports = router;