"use strict"
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
var gLIMIT_COURSES = 4
var gPopularCourses = []
var gTrendingCourses = []
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading()
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Gọi Api
    processApiToGetCourses()
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm gọi Api để lấy các khóa học
function processApiToGetCourses() {
    $.ajax({
        url: gBASE_URL + "/courses/",
        type: "GET",
        // async: false,
        success: function (responseObject) {
            // Xử lý hiển thị
            // Khóa học Most Popular
            displayPopularCourses(responseObject)
            // Khóa học Trending
            displayTrendingCourses(responseObject)
        },
        error: function (paramErr) {
            console.log(paramErr.status);
        }
    })
}
// Hàm hiển thị khóa học Most Popular
function displayPopularCourses(paramObject) {
    $("#courses-popular").html("")
    for (var bI = 0; bI < paramObject.length; bI++) {
        if (paramObject[bI].isPopular == true) {
            gPopularCourses.push(paramObject[bI])
        }
    }
    for (var bI = 0; bI < gLIMIT_COURSES; bI++) {
        // console.log(gPopularCourses)
        var bHtml = `
        <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 div-courses">
                        <div class="div-card">
                            <img class="courses-img" src="${gPopularCourses[bI].coverImage}" alt="img-courses">
                            <div class="div-border">
                                <div class="div-card-body">
                                    <h6>${gPopularCourses[bI].courseName}</h6>
                                    <div class="div-p-card">
                                        <i class="fa-regular fa-clock"></i>
                                        <p class="p-card">${gPopularCourses[bI].duration}</p>
                                        <p class="p-card">${gPopularCourses[bI].level}</p>
                                    </div>
                                    <div class="div-price-card">
                                        <p>$${gPopularCourses[bI].discountPrice}<span>$${gPopularCourses[bI].price}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="div-border-footer">
                                <div class="div-card-footer">
                                    <img class="img-teacher" src="${gPopularCourses[bI].teacherPhoto}" alt="avatar">
                                    <p>${gPopularCourses[bI].teacherName}</p>
                                </div>
                                <i class="fa-regular fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
        `
        $("#courses-popular").append(bHtml)
    }
}
// Hàm hiển thị khóa học Trending
function displayTrendingCourses(paramObject) {
    $("#courses-trending").html("")
    for (var bI = 0; bI < paramObject.length; bI++) {
        if (paramObject[bI].isTrending == true) {
            gTrendingCourses.push(paramObject[bI])
        }
    }
    for (var bI = 0; bI < gLIMIT_COURSES; bI++) {
        // console.log(gTrendingCourses)
        var bHtml = `
        <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3 div-courses">
                        <div class="div-card">
                            <img class="courses-img" src="${gTrendingCourses[bI].coverImage}" alt="img-courses">
                            <div class="div-border">
                                <div class="div-card-body">
                                    <h6>${gTrendingCourses[bI].courseName}</h6>
                                    <div class="div-p-card">
                                        <i class="fa-regular fa-clock"></i>
                                        <p class="p-card">${gTrendingCourses[bI].duration}</p>
                                        <p class="p-card">${gTrendingCourses[bI].level}</p>
                                    </div>
                                    <div class="div-price-card">
                                        <p>$${gTrendingCourses[bI].discountPrice}<span>$${gTrendingCourses[bI].price}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="div-border-footer">
                                <div class="div-card-footer">
                                    <img class="img-teacher" src="${gTrendingCourses[bI].teacherPhoto}" alt="avatar">
                                    <p>${gTrendingCourses[bI].teacherName}</p>
                                </div>
                                <i class="fa-regular fa-bookmark"></i>
                            </div>
                        </div>
                    </div>
        `
        $("#courses-trending").append(bHtml)
    }
}