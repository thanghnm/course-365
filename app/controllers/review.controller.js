const courseModel = require("../models/course.model");
const reviewModel = require("../models/review.model");
const mongoose = require("mongoose");

const createReviewOfCourse = async (req,res)=>{
// Thu thập dữ liệu
    const newReview = {
        stars,
        note,
        courseId
    } =req.body
    // Kiểm tra dữ liệu
    if(!stars){
        return res.status(404).json({
            message:"Star is required"
        })
    }
    if(!note){
        return res.status(404).json({
            message:"note is required"
        })
    }
    
    try {
        const result = await reviewModel.create(newReview);
        const updateCourseReview = await courseModel.findByIdAndUpdate(courseId,{
            $push:{reviews:result._id}
        })
        return res.status(201).json({
            message:"create review successfully",
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
module.exports={createReviewOfCourse}