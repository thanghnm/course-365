//B1: Import thư viện express
//import express from express
const express = require('express');

const mongoose = require('mongoose');

//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;
const path = require("path")
const courseRouter = require("./app/routes/course.router");
const reviewRouter = require("./app/routes/review.router");

//Cấu hình để sử dụng json
app.use(express.json());

// Middleware
// Middleware console log ra thời gian hiện tại
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
});

// Middleware console log ra request method
app.use((req, res, next) => {
    console.log("Request method:", req.method);

    next();
})
// Hiển thị hình ảnh
app.use(express.static(__dirname + "/views"))
// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb+srv://myDB:Matkhau1@cluster0.x9asdt5.mongodb.net/CRUD_Course365")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });
// Router 
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"))
})

// Sử dụng router 
app.use("/api/v1/courses", courseRouter);
app.use("/api/v1/reviews", reviewRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})
